% Elena Arcari <earcari@ethz.ch>


clc
close all;
clear all;

n = 2; %state dimension
m = 1; %input dimension

maxNumCompThreads(2) 

%% Disturbance

Q_w = [0.3, 0.5; 0.5, 1];

T_sim = 15; %simulation horizon
N = 30; %prediction horizon

T = T_sim + N; 

Acorr = [0.3, 0.001; 0, 0.05];
Qcorr = Q_w;%[0.001, 0; 0, 0.001];
A0c = zeros(size(Acorr,1) * T, size(Acorr,2) );
for i = 1:1:T
    A0c((i-1)*2 + 1: i*2, :) = Acorr ^ i;
end

Abarc = zeros(size(Acorr,1) * T, size(Acorr,2) * T );
for i = 1:1:T
    temp = zeros(size(A0c));
    idx_A0 = 1;
    for ii = 1:1:T
        if ii < i
            temp((ii-1)*2 + 1: ii*2, :)  = zeros(2);
        elseif ii == i
            temp((ii-1)*2 + 1: ii*2, :) = eye(2);
        else
            temp((ii-1)*2 + 1: ii*2, :) = A0c((idx_A0-1)*2 + 1: idx_A0*2, :);
            idx_A0 = idx_A0 + 1;
        end
    end
    Abarc(:, (i-1)*2 + 1: i*2 ) = temp;
end
w_var = Abarc * kron(eye(T),Qcorr) * Abarc';

%% Uncertainty dynamics

%A,B = A0,B0 + \theta_1 * A1,B1 + \theta_2 * A2,B2 + \theta_3 * A3,B3

A_mat{1} = [1 1; 0 1]; %nominal matrix 
A_mat{2} = [0 0;0 0];  
% 
B_mat{1} = [0.5; 1]; %nominal matrix 
B_mat{2} = [0.5; 1];
% 
A_tilde_mat{1} = A_mat{2};
B_tilde_mat{1} = B_mat{2};

Ns = 1;
%cost_rsmpc1_corr = zeros(num_aalphas,num_p_levels,Ns);
%viol_rsmpc1_corr_max = zeros(num_aalphas,num_p_levels);

p_level  = 0.8;%[0.8,0.85,0.9];
aalpha = 0.4;%[0.04,0.1,0.15,0.2,0.25,0.3,0.35,0.4];
for ALPHA=1:length(aalpha)
        
        disp(['ALPHA is: ' num2str(aalpha(ALPHA))])
        
        H_theta = [1;-1];
        h_theta = [0;aalpha(ALPHA)];
        Theta = Polyhedron(H_theta,h_theta);
        theta_true = 0;

        %% Compute matrices at vertices

        vertices = Theta.V;
        [A_v,B_v] = vertex_matrices(vertices,A_mat,B_mat);
        [A_tilde_v,B_tilde_v] = vertex_tilde_matrices(vertices,A_tilde_mat,B_tilde_mat);
        %% Constraints


        % State constraints H_x*x<=h_x
        F = [1/15 0;-1/15 0;0 1/3;0 -1/3];
        f = [1;1;1;1];


        % Use MPT to define X as Polyhedron
        X=Polyhedron(F,f);

        % Input constraints H_u*u<=h_u -- dummy just for terminal set function
        G=[1/10;-1/10];
        g=[1;1];


        % Use MPT to define U as Polyhedron -- dummy just for terminal set function
        U=Polyhedron(G,g);

        %% Find stabilizing control law K 

        % cost function matrices
        Q = eye(n);
        R = eye(m);

        [~,K] = stabilizing_feedback(A_v,B_v,Q,R);

        %% Compute closed-loop matrices at vertices for error tube

        A_cl_v = cell(size(vertices,1),1);
        for i=1:size(vertices,1)
            A_cl_v{i} = A_v{i} + B_v{i}*K ;
        end

        %% Compute closed-loop matrices at vertices for terminal set

        K_f = K;
        A_cl_f_v = cell(size(vertices,1),1);
        for i=1:size(vertices,1)
            A_cl_f_v{i} = A_v{i} + B_v{i}*K_f ;
        end
        %% Initial Error tube H_ze*e <= h_ze
        H_ze = [2 1; -1 -2; 1 2; -1 2];
        %H_ze = [1 1/0.5; -1/0.5 -1; 1/0.5 1; 1/0.5 -1];
        %H_ze = [1 0; -1 0; 0 1; 0 -1];
        h_ze = ones(size(H_ze,1),1);
        Ze_bar = Polyhedron(H_ze,h_ze);

        %% Computation of \bar{f}_r, \bar{g}_r
        ze = sdpvar(n,1);

        mat1 = X.A;
        f_bar_r = [];
        for j=1:size(X.A,1)
            sol = optimize(Ze_bar.A*ze <= Ze_bar.b,-mat1(j,:)*ze,sdpsettings('solver','mosek','verbose',0));
            f_bar_r = [f_bar_r;mat1(j,:)*value(ze)];
        end

        % no input constraint -- dummy just for terminal set function
        g_bar_r = g;


        %% PRS time-varying tightening

        disp(['k-step iteration: ' num2str(1)])
        Ve_corr{1} = w_var(1:2,1:2);

        disp(['k-step iteration: ' num2str(2)])
        Ve_corr{2} = diagonal_problem(A_cl_v,w_var(1:4,1:4));
       
        k=3;
        while 1
        
        disp(['k-step iteration: ' num2str(k)])
        Ve_corr{k} = correlated_prs(A_cl_v,w_var,Ve_corr{2},k);
        
         if max(abs(eig(Ve_corr{k} - Ve_corr{k-1}))) <= 1e-6
             break; % in practice this works because we know that the error dynamics is stable, therefore at some point the worst case variance converges, no matter the nature of the additive noise.
          end

            k = k+1;

        end

        T_sim = 15; %simulation horizon
        N = 30; %prediction horizon

        T = T_sim + N; 

        real_size = k;
        A0_sol{1} = 10000*eye(2);
        for i=1:real_size
                A0_sol{i+1} = inv(Ve_corr{i});
        end
        if real_size < T
            begin = real_size;
            for i=1:T-begin
                A0_sol{begin+i} = A0_sol{begin};
            end
        end

        for P_LEVEL=1:length(p_level)
            
        disp(['P_LEVEL is: ' num2str(p_level(P_LEVEL))])
        ze = sdpvar(n,1); %stochastic error
        for i=1:real_size

        f_bar{i} = [0;0;sqrt(chi2inv(p_level(P_LEVEL),1)/A0_sol{i}(2,2));sqrt(chi2inv(p_level(P_LEVEL),1)/A0_sol{i}(2,2))]*(1/3);

        % no input constraint -- dummy just for terminal set function
        g_bar{i} = g;

        end

        if real_size < T
            begin = real_size;
            for i=1:T-begin
                f_bar{begin+i} = f_bar{begin};
                g_bar{begin+i} = g_bar{begin};
            end
        end
        
        %f_bar_s = max([f_bar{:}],[],2); % worst case constraint tightening

        %% terminal set


        tight = zeros((size(f_bar{1},1)+size(g_bar{1},1)),length(f_bar));
        for i=1:length(f_bar)
           tight(:,i) = [f_bar{i};0*g_bar{i}];
        end
        X_f = terminal_ingredients_s(A_cl_f_v,Ze_bar,X,f_bar_r,U,0*K,0*g_bar_r,tight,0); %no input constraints

        %% MPC controller

        ze_bar = Ze_bar.V;

        %optimization variables
        s = sdpvar(repmat(n,1,N),repmat(1,1,N));
        v = sdpvar(repmat(m,1,N-1),repmat(1,1,N-1));
        u = sdpvar(repmat(m,1,N-1),repmat(1,1,N-1));
        x = sdpvar(repmat(n,1,N),repmat(1,1,N));
        w = sdpvar(repmat(n,1,N-1),repmat(1,1,N-1));
        alpha = sdpvar(repmat(1,1,N),repmat(1,1,N));
        Lambda = sdpvar(size(H_ze,1),size(H_theta,1),size(ze_bar,1),N-1);
        mu_theta_var = sdpvar(1,1);
        theta_var = sdpvar(1,1);
        f_bar_var = sdpvar(repmat(4,1,N),repmat(1,1,N)); 


        constraints = [];
        objective = 0;


        for i=1:N-1

            for j=1:size(ze_bar,1)
                p = s{i} + alpha{i}*ze_bar(j,:)';
                r = K*(s{i} + alpha{i}*ze_bar(j,:)') + v{i};
                D = D_matrix(A_tilde_mat, B_tilde_mat, p,r);
                d = (A_mat{1} + B_mat{1}*K)*(s{i} + alpha{i}*ze_bar(j,:)') + B_mat{1}*v{i} - s{i+1};
                for k=1:size(H_ze,1)
                constraints = [constraints,...
                               H_ze(k,:)*D - Lambda(k,:,j,i)*H_theta == 0,...
                               Lambda(k,:,j,i)*h_theta + H_ze(k,:)*d - alpha{i+1}*h_ze(k) <= 0, ...
                               -Lambda(k,:,j,i) <= 0];
                end
            end

            constraints = [constraints, ...
                           u{i} == K*x{i} + v{i},...
                           x{i+1} == A_mat{1}*x{i} + B_mat{1}*u{i} + D_matrix(A_tilde_mat, B_tilde_mat,x{i},u{i})*mu_theta_var + w{i},...
                           X.A(3:4,2)*s{i+1}(2) <= X.b(3:4)-alpha{i+1}*f_bar_r(3:4) - f_bar_var{i+1}(3:4),...
                           alpha{i+1} >= 0,...
                           ];

            objective=objective+x{i+1}'*Q*x{i+1}+ u{i}'*R*u{i};

        end
        constraints = [constraints, ...
                      X_f.A*[s{N};alpha{N}] <= X_f.b
                      ];

        parameters_in = {x{1},s{1},alpha{1},[w{:}],mu_theta_var,[f_bar_var{:}]};
        solutions_out = {[u{:}], [v{:}], [x{:}],[s{:}],[alpha{:}]};
        mpc_controller = optimizer(constraints, objective, sdpsettings('verbose',0,'solver','gurobi','cachesolvers',1),parameters_in,solutions_out);
      
        
        %% Rollout
        
        
        sim(1:Ns) = struct('xcl', zeros(n,T_sim+1), 'ucl',zeros(m,T_sim));
        for seed=1:Ns
            
            rng(seed)
            w_samples = randn(1,size(w_var,1))*chol(w_var);
            w_samples = reshape(w_samples,2,T);

            xcl = zeros(n,T_sim+1);
            zcl = zeros(n,T_sim+1);
            ecl = zeros(n,T_sim+1);
            ucl = zeros(m,T_sim);
            vcl = zeros(m,T_sim);
            alphacl = zeros(1,T_sim+1);
            diagnostics = zeros(1,T_sim);
            scl = zeros(n,T_sim+1);

            xcl(:,1) = [10;0];
            scl(:,1) = [10;0];
            zcl(:,1) = [10;0];
            ecl(:,1) = [0;0];

            theta = -aalpha(ALPHA);
            disp(['RSMPC seed is: ' num2str(seed) ' in alpha: ' num2str(aalpha(ALPHA)) ' and ' num2str(p_level(P_LEVEL))])
            rng(seed)
            for t=1:T_sim
                
                if t==1
                    w_pred_mean = zeros(2,N-1);
                elseif t>1
                pred_cov = w_var( 2*(t-1)-1:2*(t-1+N-1) , 2*(t-1)-1:2*(t-1+N-1)); 
                AA = pred_cov(1:2,1:2);
                CC = pred_cov(1:2,3:end);
                w_pred_mean = CC' * inv(AA+Q_w) * w_samples(:,t-1);
                w_pred_mean = reshape(w_pred_mean,2,N-1);
                end

                inputs = {xcl(:,t),scl(:,t),alphacl(t),w_pred_mean,theta,[f_bar{t:t+N-1}]};

                [solutions,diagnostics(t)] = mpc_controller(inputs);

                ucl(:,t) = solutions{1}(:,1);
                vcl(:,t) = solutions{2}(:,1);
                alphacl(:,t+1) = solutions{5}(2);
                scl(:,t+1) = solutions{4}(:,2);   
                zcl(:,t+1) = A_mat{1}*zcl(:,t) + B_mat{1}*(K*zcl(:,t) + vcl(:,t))+ D_matrix(A_tilde_mat, B_tilde_mat,zcl(:,t) ,(K*zcl(:,t)+ vcl(:,t)))*theta_true;
                xcl(:,t+1) = A_mat{1}*xcl(:,t) + B_mat{1}*ucl(:,t) + D_matrix(A_tilde_mat, B_tilde_mat,xcl(:,t) ,ucl(:,t))*theta_true + w_samples(:,t); %chol(Q_w)'*randn(n,1);
                ecl(:,t+1) = xcl(:,t+1) - zcl(:,t+1);

            end
            sim(seed).xcl = xcl;
            sim(seed).ucl = ucl;

        

        %% Compute closed-loop cost

        %for t=1:T_sim
        %    cost_rsmpc1_corr(ALPHA , P_LEVEL, seed) = cost_rsmpc1_corr(ALPHA , P_LEVEL, seed) + sim(seed).xcl(:,t)'*Q*sim(seed).xcl(:,t) + sim(seed).ucl(:,t)'*R*sim(seed).ucl(:,t);
        %end
        %cost_rsmpc1_corr(ALPHA , P_LEVEL, seed) = cost_rsmpc1_corr(ALPHA , P_LEVEL, seed) + sim(seed).xcl(:,T_sim+1)'*Q*sim(seed).xcl(:,T_sim+1);
        
        end

        %[viol_rsmpc1_corr_max(ALPHA , P_LEVEL),~] = compute_violation(sim,T_sim,Ns);
        end
    

end

%% Plotting
% mpt_init
%  
len2 = linspace(X.b(2)/X.A(2,1),X.b(1)/X.A(1,1)); %constraints x2
f_bar_plot = cell2mat(f_bar);
% 
figure;
hold on
p3=plot(len2,X.b(3)/X.A(3,2)*ones(1,length(len2)),'b','LineWidth',2); %x2
plot(len2,X.b(4)/X.A(4,2)*ones(1,length(len2)),'b','LineWidth',2)
for i=1:T_sim+1
    Z_scaled = Polyhedron(Ze_bar.A,alphacl(i)*Ze_bar.b);
    Z_shift = scl(:,i) + Z_scaled;
    p0=Z_shift.plot('wire',1);
    X_cl = Polyhedron(X.A,X.b - f_bar_plot(:,i));
    p4=X_cl.plot('wire',1,'linestyle','--');
end

p2=plot(xcl(1,:),xcl(2,:),'o-');
p5=plot((scl(1,:)),(scl(2,:) ),'+');
p1=plot((zcl(1,:)),(zcl(2,:) ),'g');
xlim([-11,11])
legend([p0,p1,p2,p3,p4,p5],'robust nominal tube','closed loop z','closed loop x','original constraints','tightened constraints','closed loop s')

%%
plot_s2 = X.A([3,4],2)*scl(2,:) + alphacl.*f_bar_r(3:4);
plot_shift = X.b(3:4) - f_bar_plot(3:4,1:T_sim+1); %f_bar_s(3:4))*ones(1,T_sim+1);

figure;
hold on
p1=plot((0:T_sim),plot_s2(1,:)/X.A(3,2),'b','LineWidth',2);
p2=plot((0:T_sim),plot_shift(1,:)/X.A(3,2),'--k');
p3=plot((0:T_sim),xcl(2,:),'r');
p4=plot((0:T_sim),zcl(2,:),'g');
plot((0:T_sim),plot_s2(2,:)/X.A(4,2),'b','LineWidth',2);
plot((0:T_sim),plot_shift(2,:)/X.A(4,2),'--k');
p5=plot((0:T_sim),X.b(3)/X.A(3,2)*ones(T_sim+1,1),'k');
plot((0:T_sim),X.b(4)/X.A(4,2)*ones(T_sim+1,1),'k');
legend([p1,p2,p3,p4,p5],'robust nominal tube', 'tightened constraints','closed loop x2', 'closed loop z2','original constraints')