## Description
Robust stochastic mpc double integrator example (iid and correlated disturbances) exemplified in paper [1]

The MATLAB code relies entrirely on:
- [Multi Parametric Toolbox](https://www.mpt3.org/) + [Mosek](https://www.mosek.com/) for offline set computations. 
- [YALMIP](https://yalmip.github.io/) + [Gurobi](https://www.gurobi.com/) for the MPC problem

- Run `rsmpc_correlated.m` to execute the simulation where the system is corrupted by correlated noise sequences.
- Run `rsmpc_iid.m` to execute the simulation where the system is corrupted by i.i.d. noise sequences.

## Authors and acknowledgment
[1] E. Arcari, A. Iannelli, A. Carron, M. N. Zeilinger, “Stochastic MPC with Robustness to
Bounded Parametric Uncertainty”, IEEE Transactions on Automatic Control 2023

