function P_sol_tr = ellipsoidal_parametric_prs_tr(A,Q,T)
%ELLIPSOIDAL_PARAMETRIC_PRS Summary of this function goes here
%   Detailed explanation goes here

if T == inf

        dim = size(Q,1);
        P = zeros(dim);

        Pplus = sdpvar(dim);

        P_sol_tr{1} = P;

        k=1;
        while 1


            constraints = [];
            for i=1:size(A,1)
            bigmatrix{i} =  [Pplus - Q, A{i}*P_sol_tr{k};...
                              P_sol_tr{k}*A{i}',   P_sol_tr{k}];

            constraints = [constraints, bigmatrix{i} >= 0];
            end
            constraints = [constraints,...
                           Pplus - Q >= 0]; 
            sol = optimize(constraints,trace(Pplus),sdpsettings('solver','mosek','verbose',1)); 
            P_sol_tr{k+1} = value(Pplus);


          if max(abs(eig(P_sol_tr{k+1} - P_sol_tr{k}))) <= 1e-6
             break;
          end
            
            k = k+1;

        end
        
        
    else
        
        dim = size(Q{1},1);
        P = zeros(dim);

        Pplus = sdpvar(dim);

        P_sol_tr{1} = P;
        for k=1:T

             constraints = [];
            for i=1:size(A,1)
            bigmatrix{i} =  [Pplus - Q{k}, A{i}*P_sol_tr{k};...
                              P_sol_tr{k}*A{i}',   P_sol_tr{k}];

            constraints = [constraints, bigmatrix{i} >= 0];
            end
            constraints = [constraints,...
                           Pplus - Q{k} >= 0]; 
            sol = optimize(constraints,trace(Pplus),sdpsettings('solver','mosek','verbose',0)); 
            P_sol_tr{k+1} = value(Pplus);
        end


end
    

end

