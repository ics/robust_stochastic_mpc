function X_f = terminal_ingredients_s(A_cl_v,Ze_bar,X,f_bar_r,U,K,g_bar_r,tight,zero_terminal_set)
%TERMINAL_INGREDIENTS Summary of this function goes here
%   Detailed explanation goes here

if zero_terminal_set
    
    X_f_ = [f_bar_r;g_bar_r;-1]; %no input constraints
    x_f_ = [ones(size(X_f_,1)-1,1) - tight;0];
    X_f = Polyhedron(X_f_,x_f_);
    X_f.minHRep;
    
else
    n = size(X.A,2);
    e = sdpvar(n,1);


    F_tilde = [X.A;zeros(size(U.A,1),size(X.A,2))];
    GK_tilde = [zeros(size(X.A,1),size(K,2));U.A*K];
    X0_f_ = [];
    x0_f_ = [];
    for i=1:size(tight,2)
        X0_f_ = [X0_f_;[[F_tilde + GK_tilde,[f_bar_r;g_bar_r]];[zeros(1,size(F_tilde + GK_tilde,2)),-1]]];
        x0_f_ = [x0_f_;[ones(size(F_tilde,1),1) - tight(:,i);0]];
    end
    X0_f = Polyhedron(X0_f_,x0_f_);
    X0_f.minHRep;

    if X0_f.isEmptySet == 1
        disp("The set is empty")
        return
    end

    if X0_f.isBounded == 0
        disp("The set is unbounded")
        return
    end

    h_bar = [];
    for i=1:length(A_cl_v)
        h_bar_k = [];
        for j=1:size(Ze_bar.A,1)
        sol = optimize(Ze_bar.A*e <= Ze_bar.b, -Ze_bar.A(j,:)*A_cl_v{i}*e,sdpsettings('solver','mosek','verbose',0));
        h_bar_k = [h_bar_k; Ze_bar.A(j,:)*A_cl_v{i}*value(e)]; % size of vector equale to size of X_bar.A
        end
        h_bar = [h_bar,h_bar_k];  % number of columns equal to the number of Theta vertices
    end

    Xi_f = X0_f;
    iter = 0
    flag = 1;
    while flag

        Xiplus_f_tilde_ = [];
        for i=1:length(A_cl_v)
            Xiplus_f_tilde_ = [Xiplus_f_tilde_;[Ze_bar.A*A_cl_v{i},h_bar(:,i),-Ze_bar.A, -ones(size(Ze_bar.A,1),1)]];
        end
        Xiplus_f_tilde_ = [Xiplus_f_tilde_;[X0_f.A,zeros(size(X0_f.A,1),size(Xi_f.A,2));zeros(size(Xi_f.A,1),size(X0_f.A,2)),Xi_f.A]];

        xiplus_f_tilde_ = [-repmat(zeros(size(Ze_bar.A,1),1),length(A_cl_v),1);X0_f.b;Xi_f.b];
        Xiplus_f_tilde = Polyhedron(Xiplus_f_tilde_,xiplus_f_tilde_);
        Xiplus_f_tilde.minHRep;
        Xiplus_f = projection(Xiplus_f_tilde,1:n+1);
        Xiplus_f.minVRep;

    %     figure
    %     hold on
    %     Xi_f.plot('wire',1)
    %     Xiplus_f.plot('color','b')
    %     title('stochastic')

        vi = Xi_f.V;
        vplusi = Xiplus_f.V;

        check1 = [];
        for i=1:size(vi,1)
            check1 = [check1;Xiplus_f.contains(vi(i,:)')];
        end

        check2 = [];
        for i=1:size(vplusi,1)
            check2 = [check2;Xi_f.contains(vplusi(i,:)')];
        end

        if size(vi,1) == size(vplusi,1)
        check = (max(sqrt(sum(Xiplus_f.V - Xi_f.V).^2)) <= 1e-6);
        else
        check = 0;
        end

        if ((min(check1) && min(check2)) == 1) || check
            flag = 0;
        elseif ((min(check1) && min(check2)) ~= 1)
            Xi_f = Xiplus_f;
            iter = iter + 1
        end

    end

    X_f = Xi_f;
end

end

