function [P_sol,exit_code] = off_diagonal_problem(Q)
%ELLIPSOIDAL_PARAMETRIC_PRS Summary of this function goes here
%   Detailed explanation goes here


        
dim = size(Q{1},1);

P = sdpvar(dim);

constraints = [];
for i=1:size(Q,1)
    constraints = [constraints, P - Q{i} >= 0];
end
sol = optimize(constraints,trace(P),sdpsettings('solver','mosek','verbose',0));
P_sol = value(P);

% constraints = [];
% for i=1:size(Q,1)
%     bigmatrix{i} =  [ P   ,   P*Q{i} ;...
%                    Q{i}*P,   Q{i}  ];
% 
%     constraints = [constraints, bigmatrix{i} >= 0];
% end
% sol = optimize(constraints,-logdet(P),sdpsettings('solver','mosek','verbose',0));
% 
% P_sol2 = pinv(value(P));
exit_code = sol.problem;



end


