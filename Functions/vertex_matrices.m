function [A_v,B_v] = vertex_matrices(vertices,A,B)
%VERTEX_MATRICES Summary of this function goes here
%   Detailed explanation goes here


A_v = cell(size(vertices,1),1);
B_v = cell(size(vertices,1),1);
for i=1:size(vertices,1)
     A_v{i} = A{1};
     B_v{i} = B{1};
    for j=1:size(vertices,2)
        A_v{i} = A_v{i} + vertices(i,j)*A{j+1};
        B_v{i} = B_v{i} + vertices(i,j)*B{j+1};
    end
end


end

