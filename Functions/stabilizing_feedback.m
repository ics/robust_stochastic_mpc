function [P,K] = stabilizing_feedback(A,B,Q,R)
%STABILIZING_FEEDBACK Summary of this function goes here
%   Detailed explanation goes here

n = size(A{1},1);
m = size(B{1},2);

X_PK=sdpvar(n,n);
Y_PK=sdpvar(m,n);
objective_PK=-logdet(X_PK);
constraints_PK=[];
for i=1:length(A)
    % Riccati equation
    constraints_PK=[constraints_PK,...
        [X_PK,(A{i}*X_PK+B{i}*Y_PK)', Q^0.5*X_PK, Y_PK'*R^0.5;...
        (A{i}*X_PK+B{i}*Y_PK),X_PK,zeros(n), zeros(n,m);...
        (Q^0.5*X_PK)',zeros(n),eye(n),zeros(n,m);...
        (Y_PK'*R^0.5)',zeros(m,n),zeros(m,n),eye(m)]>=0];
end

sol=optimize(constraints_PK,objective_PK,sdpsettings('solver','mosek','verbose',0));
K =value(Y_PK)*inv(value(X_PK));
P = inv(value(X_PK));


end

