function [P_sol,exit_code] = off_diagonal_problem_lp(Q)
%ELLIPSOIDAL_PARAMETRIC_PRS Summary of this function goes here
%   Detailed explanation goes here


        
dim = size(Q{1},1);

P = sdpvar(dim);
T = sdpvar(dim);

constraints = [];
for i=1:size(Q,1)
    for j=1:dim
        constraints = [constraints, P(j,j) - Q{i}(j,j) >=0,...
                                    P(j,j) >= T(j,[1:j-1,j+1:dim])*ones(dim-1,1)...
                                    -T(j,[1:j-1,j+1:dim]) <= P(j,[1:j-1,j+1:dim]) - Q{i}(j,[1:j-1,j+1:dim])...
                                    P(j,[1:j-1,j+1:dim]) - Q{i}(j,[1:j-1,j+1:dim]) <= T(j,[1:j-1,j+1:dim])];
    end
end
sol = optimize(constraints,trace(P),sdpsettings('solver','mosek','verbose',0));
P_sol = value(P);
exit_code = sol.problem;
end


