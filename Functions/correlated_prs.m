function Ve_k = correlated_prs(A_cl_v,cov_w,Ve_2,kstep)
%CORRELATED_PRS Summary of this function goes here
%   Detailed explanation goes here

dim = size(A_cl_v{1},1);
D1 = Ve_2;
% OD = cov_w(1:2*dim,(2*dim+1):end);
% D2 = cov_w((2*dim+1):end,(2*dim+1):end);
OD = cov_w(1:2*dim,(2*dim+1):kstep*dim);
D2 = cov_w((2*dim+1):kstep*dim,(2*dim+1):kstep*dim);
for k=1:kstep-2
       
    temp = cell(size(A_cl_v,1),1);
    for i=1:size(A_cl_v,1)
        temp{i} = [D1, [A_cl_v{i},eye(dim)]*OD; OD'*[A_cl_v{i}';eye(dim)] , D2];
    end
    
    %disp('Solving off diagonal problem')
    %[solution,exit_code] = off_diagonal_problem_dd(temp,dim);
    %[solution,exit_code] = off_diagonal_problem(temp);
    [solution,exit_code] = off_diagonal_problem_lp(temp);
    if exit_code == 0
    elseif exit_code == 1
     disp('Solver thinks it is infeasible')
     break
    else
     disp('Something else happened')
     break
    end

    OD = solution(1:2*dim,(2*dim+1):end);
    
    D2 = solution((2*dim+1):end,(2*dim+1):end);
    
    %disp('Solving diagonal problem')
    [D1,exit_code] = diagonal_problem(A_cl_v, solution(1:2*dim,1:2*dim));
    if exit_code == 0
    elseif exit_code == 1
     disp('Solver thinks it is infeasible')
     break
    else
     disp('Something else happened')
     break
    end

end

Ve_k = D1;
end

