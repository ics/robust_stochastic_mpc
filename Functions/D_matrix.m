function D = D_matrix(A,B,x,u)
%D_MATRIX Summary of this function goes here
%   Detailed explanation goes here

D = [];
for i=1:length(A)
    D = [D, A{i}*x + B{i}*u];
end

end

