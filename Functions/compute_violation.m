function [viol_max,viol] = compute_violation(simulation,T_sim,Ns)
%COMPUTE_VIOLATION Summary of this function goes here
%   Detailed explanation goes here
viol = zeros(T_sim+1,1);

for t=1:T_sim+1
    for i=1:Ns
        if abs(simulation(i).xcl(2,t)) > 3
            viol(t) = viol(t) + 1;
        end
    end
    viol(t) = viol(t)/Ns;

end
viol_max = max(viol)*100;

end

