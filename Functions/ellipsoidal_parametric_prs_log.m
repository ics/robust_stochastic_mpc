function P_sol_log = ellipsoidal_parametric_prs_log(A,Q,T)
%ELLIPSOIDAL_PARAMETRIC_PRS Summary of this function goes here
%   Detailed explanation goes here

if T == inf

        dim = size(Q,1);
        P = 10000*eye(dim);

        Pplus = sdpvar(dim);

        P_sol_log{1} = P;

        k=1;
        while 1


            constraints = [];
            for i=1:size(A,1)
            bigmatrix{i} =  [Pplus, Pplus*Q, Pplus*A{i};...
                            Q*Pplus, Q     , zeros(dim);...
                            A{i}'*Pplus, zeros(dim),P_sol_log{k}];

            constraints = [constraints, bigmatrix{i} >= 0];
            end
            sol = optimize(constraints,-logdet(Pplus),sdpsettings('solver','mosek','verbose',0)); 
            P_sol_log{k+1} = value(Pplus);


          if max(abs(eig(P_sol_log{k+1} - P_sol_log{k}))) <= 1e-6
             break;
          end
            
            k = k+1;

        end
        
        
    else
        
        dim = size(Q{1},1);
        P = 10000*eye(dim);

        Pplus = sdpvar(dim);

        P_sol_log{1} = P;

        for k=1:T

            constraints = [];
            for i=1:size(A,1)
            bigmatrix{i} =  [Pplus, Pplus*Q{k}, Pplus*A{i};...
                            Q{k}*Pplus, Q{k}     , zeros(dim);...
                            A{i}'*Pplus, zeros(dim),P_sol_log{k}];

            constraints = [constraints, bigmatrix{i} >= 0];
            end
            sol = optimize(constraints,-logdet(Pplus),sdpsettings('solver','mosek','verbose',1)); 
            P_sol_log{k+1} = value(Pplus);


        end
    
end

end

