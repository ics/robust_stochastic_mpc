function [P_sol,exit_code] = diagonal_problem(A,P)
%ELLIPSOIDAL_PARAMETRIC_PRS Summary of this function goes here
%   Detailed explanation goes here


        
dim = size(A{1},1);

Pplus = sdpvar(dim);

P11 = P(1:dim,1:dim);
P12 = P(1:dim,(dim+1):end);
P21 = P((dim+1):end,1:dim);
P22 = P((dim+1):end,(dim+1):end);
P11_inv = pinv(P11);

constraints = [];
for i=1:size(A,1)
    bigmatrix{i} =  [                 Pplus           ,   Pplus*(A{i}*P12 + P21*A{i}' + P22),  Pplus*A{i} ;...
                    (A{i}*P12 + P21*A{i}' + P22)*Pplus,     (A{i}*P12 + P21*A{i}' + P22)    ,  zeros(dim) ;...
                                   A{i}'*Pplus        ,               zeros(dim)            ,   P11_inv  ];

    constraints = [constraints, bigmatrix{i} >= 0];
end
sol = optimize(constraints,-logdet(Pplus),sdpsettings('solver','mosek','verbose',0)); 
P_sol = pinv(value(Pplus));
exit_code = sol.problem;


end


