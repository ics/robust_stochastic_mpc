function [P_sol,exit_code] = off_diagonal_problem_dd(Q,n)
%ELLIPSOIDAL_PARAMETRIC_PRS Summary of this function goes here
%   Detailed explanation goes here


        
dim = size(Q{1},1);

P11 = diag(sdpvar(n,1));
P22 = diag(sdpvar(dim-n,1));
P12 = sdpvar(n,dim-n);

constraints = [];
for i=1:size(Q,1)
    matrix = [P11 , P12 - Q{i}(1:n,n+1:end);... 
              P12' - Q{i}(n+1:end,1:n), P22];
    constraints = [constraints, matrix >= 0 ];
end
sol = optimize(constraints,trace(P11) + trace(P22) ,sdpsettings('solver','mosek','verbose',0));
P_sol = [value(P11) + Q{i}(1:n,1:n) , value(P12); value(P12)', value(P22) + Q{i}(n+1:end,n+1:end)];
exit_code = sol.problem;



end


